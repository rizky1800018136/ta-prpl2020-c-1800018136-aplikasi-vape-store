<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>vape store</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
  <link href="img/favicon.png" rel="icon">
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Titillium+Web:400,600,300,200&subset=latin,latin-ext" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="lib/fancybox/fancybox.css" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="css/style.css" rel="stylesheet">

  <link rel="prefetch" href="img/zoom.png">
</head>

<body data-spy="scroll" data-offset="58" data-target="#navigation">

  <!-- Fixed navbar -->
  <div id="navigation" class="navbar navbar-fixed-top">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
          <i class="fa fa-bars"></i>
        </button>
      </div>
      <div class="navbar-collapse collapse">
        <ul class="nav navbar-nav">
          <li><a target="_blank" href="index.php" class="fa fa-home"></a></li>
          <li><a target="_blank" href="088228975080" class="fa fa-phone"></a></li>
          <li><a target="_blank" href="https://myaccount.google.com/u/1/?utm_source=sign_in_no_continue&pageId=none" class="fa fa-envelope"></a></li>
          <li><a target="_blank" href="https://twitter.com/home" class="fa fa-twitter"></a></li>
          <li><a target="_blank" href="https://www.instagram.com/accounts/login/?hl=id" class="fa fa-instagram"></a></li>
          <li><a target="_blank" href="https://id-id.facebook.com/login/" class="fa fa-facebook" ></a></li>
          <li><a target="_blank" href="#" class="fa fa-comments"></a></li>
        </ul>
      </div>
      <!--/.nav-collapse -->
    </div>
  </div>

  <!-- === MAIN Background === -->
  <div class="slide story" id="intro" data-slide="1">
    <div class="container">
      <div id="home-row-1" class="row clearfix">
        <div class="col-12">
          <h1 class="font-semibold">Vape <span class="font-thin">STORE</span></h1>
          <h4 class="font-thin">We are an <span class="font-semibold">independent interactive agency</span> based in VAPE Shop.</h4>
          <br>
          <br>
        </div>
        <!-- /col-12 -->
      </div>
      <!-- /row -->
      <div id="home-row-2" class="row clearfix">

        <div class="col-12 col-sm-4">
          <a href="costumer.php" class="navigation-slide-a"><div class="home-hover navigation-slide"><img src="img/ca.png"></div><span>COSTUMER</span></a>
        </div>

        <div class="col-12 col-sm-4">
          <a href="barang.php" class="navigation-slide-a"><div class="home-hover navigation-slide"><img src="img/e.png"></div><span>BARANG</span></a>
        </div>

        <div class="col-12 col-sm-4">
          <a href="karyawan.php" class="navigation-slide-a"><div class="home-hover navigation-slide"><img src="img/kk.png"></div><span>KARYAWAN</span></a>
        </div>
      </div>
      <!-- /row -->
    </div>
    <!-- /container -->
  </div>

  <!-- JavaScript Libraries -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
  <script src="lib/easing/easing.min.js"></script>
  <script src="lib/php-mail-form/validate.js"></script>
  <script src="lib/fancybox/fancybox.js"></script>

  <!-- Template Main Javascript File -->
  <script src="js/main.js"></script>

</body>
</html>
